#include<graphics.h>
#include<conio.h>
#include<math.h>


#define High 480                                                         //游戏画面尺寸
#define Width 640
#define PI 3.14159

int main()
{
	initgraph(Width, High);                                               //初始化640*480的绘画窗口
	double center_x, center_y;                                               //中心点的坐标，也是钟表的中心
	center_x = Width / 2;
	center_y = High / 2;
	double secondLength = Width / 7;                                            //秒针的长度
	double minuteLength = Width / 6;                                            //分针的长度
	double hourLength = Width / 5;                                              //时针的长度

	double secondEnd_x, secondEnd_y;                                         //秒针的终点
	double minuteEnd_x, minuteEnd_y;                                         //分针的终点
	double hourEnd_x, hourEnd_y;                                             //时针的终点

	double secondAngle;                                                   //秒针对应的角度
	double minuteAngle;                                                   //分针对应的角度
	double hourAngle;                                                     //时针对应的角度

	SYSTEMTIME ti;                                                       //定义变量保存当前时间


	while (1)
	{
		GetLocalTime(&ti);                                               //获取当前时间

		//秒针角度的变化
		secondAngle = ti.wSecond * 2 * PI / 60;                                  //一圈一共2*PI，一圈60秒，一秒钟秒针走
																		 //角度为2*PI/60

		//分针角度的变化
		minuteAngle = ti.wMinute * 2 * PI / 60;


		//时针角度的变化
		hourAngle = ti.wHour * 2 * PI / 12;


		//由角度决定的秒针端点坐标
		secondEnd_x = center_x + secondLength * sin(secondAngle);
		secondEnd_y = center_y - secondLength * cos(secondAngle);

		//由角度决定的分针端点坐标
		minuteEnd_x = center_x + minuteLength * sin(minuteAngle);
		minuteEnd_y = center_y + minuteLength * cos(minuteAngle);

		//由角度决定的时针端点坐标
		hourEnd_x = center_x + hourLength * sin(hourAngle);
		hourEnd_y = center_y + hourLength * cos(hourAngle);

		setlinestyle(PS_SOLID, 2);                                        //画实线，宽度为两个像素
		setcolor(WHITE);
		line(center_x, center_y, secondEnd_x, secondEnd_y);                 //画秒针


		setlinestyle(PS_SOLID, 4);
		setcolor(BLUE);
		line(center_x, center_y, minuteEnd_x, minuteEnd_y);                 //画分针


		setlinestyle(PS_SOLID, 6);
		setcolor(RED);
		line(center_x, center_y, hourEnd_x, hourEnd_y);                     //画时针


		Sleep(10);

		setcolor(BLACK);
		setlinestyle(PS_SOLID, 2);
		line(center_x, center_y, secondEnd_x, secondEnd_y);                 //隐藏前一帧的秒针
		setlinestyle(PS_SOLID, 4);
		line(center_x, center_y, minuteEnd_x, minuteEnd_y);                 //隐藏前一帧的分针
		setlinestyle(PS_SOLID, 6);
		line(center_x, center_y, hourEnd_x, hourEnd_y);                     //隐藏前一帧的时针



	}


	_getch();                                                             //按任意键继续
	closegraph();                                                        //关闭绘图窗口
	return 0;



}
