#include <graphics.h>
#include <conio.h>
#include <math.h>
#include <stdio.h>
#include <Windows.h>
#include <stdlib.h>


//引用Windows multimedia API
#pragma comment(lib,"Winm.lib")

#define High 864    // 游戏尺寸
#define Width 591

IMAGE img_bk;                //背景图片
int position_x, position_y;            //飞机的位置
IMAGE img_planeNormal1, img_planeNormal2; //飞机的图片
int bullet_x, bullet_y; //子弹的位置
IMAGE img_bullet1, img_bullet2; //子弹的图片


void startup()
{
	initgraph(Width, High);
	loadimage(&img_bk, "D:\\作业\\C 语言程序设计\\飞机大战图片音乐素材\\background.jpg");
	loadimage(&img_planeNormal1, "D:\\作业\\C 语言程序设计\\飞机大战图片音乐素材\\planeNormal_1.jpg");
	loadimage(&img_planeNormal2, "D:\\作业\\C 语言程序设计\\飞机大战图片音乐素材\\planeNormal_2.jpg");
	loadimage(&img_bullet1, "D:\\作业\\C 语言程序设计\\飞机大战图片音乐素材\\bullet1.jpg");
	loadimage(&img_bullet2, "D:\\作业\\C 语言程序设计\\飞机大战图片音乐素材\\bullet2.jpg");
	position_x = High * 0.7;
	position_y = Width * 0.5;
	bullet_x = position_x;
	bullet_y = -85;
	BeginBatchDraw();
}

void show()
{
	putimage(0, 0, &img_bk);         //显示背景
	putimage(position_x - 50, position_y - 60, &img_planeNormal1, NOTSRCERASE);     //显示飞机
	putimage(position_x - 50, position_y - 60, &img_planeNormal2, SRCINVERT);
	putimage(bullet_x - 7, bullet_y, &img_bullet1, NOTSRCERASE);    //显示子弹
	putimage(bullet_y - 7, bullet_y, &img_bullet1, SRCINVERT);

	FlushBatchDraw();

}
void updateWithoutInput()
{
	if (bullet_y > -25)
		bullet_y = bullet_y - 3;
}
void updateWithInput()
{
	MOUSEMSG m;           //定义鼠标消息

	while (MouseHit());       //这个函数用于检测当前是否有鼠标消息
	{
		m = GetMouseMsg();
		if (m.uMsg == WM_MOUSEMOVE)
		{
			//飞机的位置等于鼠标所在的位置
			position_x = m.x;
			position_y = m.y;
		}
		else if (m.uMsg == WM_LBUTTONDOWN)
		{
			//按下鼠标左键发射子弹
			bullet_x = position_x;
			bullet_y = position_y - 85;
		}
	}

}
void gameover()
{
	EndBatchDraw();
	_getch();
	closegraph();
}
int main()
{
	startup();               //数据的初始化
	while (1)            //游戏循环执行
	{
		show();           //显示画面
		updateWithoutInput();   //与用户输入无关的更新
		updateWithInput();       //与用户输入有关的更新

	}
	gameover();         //游戏结束，进行后续处理
	return 0;
}

