#include<graphics.h>
#include<conio.h>
#include<math.h>
#include<Windows.h>


constexpr auto high = 480              //游戏画面尺寸;
constexpr auto width = 640;
constexpr auto pi = 3.14159;

int main()
{
	initgraph(width, high);        //初始化640*480的绘图窗口
	int  center_x, center_y;           //中心点的坐标，也是钟表的中心
	center_x = width / 2;
	center_y = high / 2;
	int secondlenth = width/7;          //秒针的长度
	int minutelength = width / 6;     //分针的长度
	int hourlength = width / 5;     //时针的长度


	int secondend_x, secondend_y;         //秒针的终点
	int minuteend_x, minuteend_y;           //分针的终点
	int hourend_x, hourend_y;                //时钟的终点
	float secondangle;     //秒针对应的角度
	float minuteangle;   //分针对应的角度
	float hourangle;     //时针对应的角度

	SYSTEMTIME ti;        //定义变量保存当前时间

	BeginBatchDraw();
	while (1)
	{
		//绘制一个简单的表盘
		setlinestyle(PS_SOLID, 1);
		setcolor(WHITE);
		circle(center_x, center_y, width / 4);

		//画刻度
		int x, y, i;
		for (i = 0; i < 60; i++)
		{
			x = center_x + int(width / 4.3*sin(pi * 2 * i / 60));
			y = center_y + int(width / 4.3*cos(pi * 2 * i / 60));

			if (i % 15 == 0)
				bar(x - 5, y - 5, x + 5, y + 5);
			else if (i % 5 == 0)
				circle(x, y, 3);
			else
				putpixel(x, y, WHITE);
		}
		outtextxy(center_x-25,center_y+width/6,"我的时钟")；

		GetLocalTime(&ti);         //获取当前时间
		//秒针角度的变化
		secondangle = ti.wSecond * 2 * pi / 60;  //一圈一共2*pi，一圈60秒，一秒钟秒针走过的角度为2*pi/60
		//分针角度的变化
		minuteangle = ti.wMinute * 2 * pi * 60;   //一圈一共2*pi，一圈60分，一秒钟分针走过的角度为2*pi/60
		//时针角度的变化
		hourangle = ti.wHour * 2 * pi / 12;    //一圈共2*pi，一圈12小时，一秒钟分针走过的角度为2*pi/12

		//由角度决定的秒针终点坐标
		secondend_x = center_x + secondlenth*sin(secondangle);
		secondend_y = center_y - secondlenth*cos(secondangle);
		//由角度决定的分针端点坐标
		minuteend_x = center_x + minutelength * sin(minuteangle);
		minuteend_y = center_y - minutelength * cos(minuteangle);
		//由角度决定的时针终点坐标
		hourend_x = center_x + hourlength * sin(hourangle);
		hourend_y = center_y - hourlength * cos(hourangle);





		//画秒针
		setlinestyle(PS_SOLID, 2);      //画实线，宽度为两个像素
		setcolor(YELLOW);
		line(center_x, center_y, secondend_x, secondend_y);
		//画分针
		setlinestyle(PS_SOLID, 4);
		setcolor(BLUE);
		line(center_x, center_y, minuteend_x, minuteend_y);
		//画时针
		setlinestyle(PS_SOLID, 6);
		setcolor(RED);
		line(center_x, center_y, hourend_x, hourend_y);

		FlushBatchDraw();
		Sleep(10);

		setcolor(BLACK); 
		setlinestyle(PS_SOLID, 2);
		line(center_x, center_y, secondend_x, secondend_y);//隐藏前一秒的秒针
		setlinestyle(PS_SOLID, 5);
		line(center_x, center_y, minuteend_x, minuteend_y);  //隐藏前一帧的分针
		setlinestyle(PS_SOLID, 10);
		line(center_x, center_y, hourend_x, hourend_y);     //隐藏前一帧的时针

	



	}
	EndBatchDraw();
	_getch();      //按任意键继续
	closegraph();
	return 0;
}