#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include<windows.h>

#define High 25       //游戏画面尺寸
#define Width 50

//全局变量
int position_x,position_y;   // 飞机位置
int canvas[High][Width] = {0}; //二维数组存储游戏画中对应的元素
                      //0为空格，1为飞机
void gotoxy(int x,int y)              //将光标移动到(x,y)位置
{
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD pos;
	pos.X = x;
	pos.Y = y;
	SetConsoleCursorPosition(handle,pos);
 } 
 void startup()       //数据的初始化
 {
 	position_x = High/2;
 	position_y = Width/2;
 	canvas[position_x][position_y] = 1;
  } 
  void show()       //显示画面 
  {
  	gotoxy(0,0);          //光标移动到原点位置，一下重画清屏
	    int i, j;
		for(i=0;i<High;i++)
		{
			for(j=0;j<Width;j++)
			{
				if(canvas[i][j]==0)
				    printf(" ");     //输出空格
				else if (canvas[i][j]==1)
				    printf("*");           //输出飞机* 
				    
			}
			printf("\n"); 
		 } 
   } 
void updateWithoutInput()             //与用户输入无关的更新
{
	
 } 
void updateWithInput()             //与用户输入有关的更新
{
	char input;
	if(kbhit())
	{
		input = getch();       //判断是否有输入
		if(input == 'a') //根据用户的不同输入来移动，不必输入回车
		{
			canvas[position_x][position_y] = 0;
			position_y--;                  //位置左移
			canvas[position_x][position_y] = 1; 
		 } 
		 else if(input == 'd')
		 {
		 	canvas[position_x][position_y] = 0;
			position_y++;                  //位置右移
			canvas[position_x][position_y] = 1; 
		 }
		 else if(input == 'w')
		 {
		 	canvas[position_x][position_y] = 0;
			position_x--;                  //位置上移
			canvas[position_x][position_y] = 1; 
		 }
		 else if(input == 's')
		 {
		 	canvas[position_x][position_y] = 0;
			position_x++;                  //位置下移
			canvas[position_x][position_y] = 1; 
		 }
	}
 }
int main()
{
	startup();
	while(1)           //数据初始化
	{
		show();      //显示画面
		updateWithoutInput();     //与用户输入无关的更新
		updateWithInput(); // 与用户输入有关的更新 
	 } 
	 return 0; 
 } 















 
 
 
 
 
 
 
 
 
 
