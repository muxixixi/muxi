#include<stdio.h>
#include<stdlib.h>
#include<conio.h> 
#include<windows.h>
int position_x,position_y;          //飞机的位置 
int bullet_x,bullet_y;        //子弹位置 
int enemy_x,enemy_y; 
int high, width;                    //游戏画面尺寸 
int score;                  //得分 
void gotoxy(int x,int y)
{
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD pos;
	pos.X = x;
	pos.Y = y;
	SetConsoleCursorPosition(handle,pos);
}
void startup()                     //数据初始化 
{
	high = 20;
	width = 30;
	position_x = high/2;
	position_y = width/2;
	bullet_x = -1;
	bullet_y = position_y;
	enemy_x = 0;
	enemy_y = position_y;
	score = 0; 
 } 
 
void show()    //显示画面 
{
	 gotoxy(0,0);
	 int i, j;
	 for(i=0;i<high;i++)
	 {
	 	for(j=0;j<width;j++)
	 	{
	 		if((i==position_x)&&(j==position_y))
	 		    printf("*");         //输出飞机
	 		else if((i==enemy_x)&&(j==enemy_y))
	 		    printf("@");
		    else if((i==bullet_x)&&(j==bullet_y)) 
		        printf("|");
	 		else
	 		    printf(" ");
		 }
		 printf("\n");
	 }
	 printf("得分: %d\n", score);
}
void updateWithoutInput()             //输入与用户无关的更新 
{
	if(bullet_x>-1) 
	    bullet_x--;
	if((bullet_x==enemy_x)&&(bullet_y==enemy_y))
	{
		score++;
		enemy_x = -1;        //分数加一 
		enemy_y = rand()%width;     
		bullet_x = -2;         //子弹无效 
	}
	if(enemy_x>high)                //敌机跑出显示屏 
	{
		enemy_x = -1;          //产生新的飞机 
		enemy_y = rand()%width;
	}
	//用来控制敌机向下移动的速度，每隔几次循环才移动一次敌机
	//这样修改，虽然用户按键的互交速度还是很快，但NPC的移动显示可以降速
	static int speed = 0;
	if(speed<10)
	    speed++;
	if(speed==10)
	    enemy_x++;
		speed = 0; 
}
void updateWithInput()            //与用户输入有关的更新 
{
	char input; 
	if(kbhit())                   //判断是否有输入 
	{
		input =  getch();            //根据用户的不同输入来移动，不必输入回车 
		if(input == 'a')
		    position_y--;            //位置右移 
		if(input == 'd')
		    position_y++;           //位置左移 
	    if(input == 'w')
	        position_x--;        //位置上移 
	    if(input == 's')
	        position_x++;             //位置下移
		if(input == ' ')                        //发射子弹
		 {
		 	bullet_x = position_x-1;        //发射子弹的初始位置在飞机的正上方
			bullet_y = position_y; 
		  } 
	 } 
}

int main()
{
	startup();        //数据的初始化 
	while(1)              //游戏循环执行 
	{
		show();                 //显示画面 
		updateWithoutInput();     //与用户输入无关的更新 
		updateWithInput();           //与用户输入有关的更新 
	}

	return 0;
}














